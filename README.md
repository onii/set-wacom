# Set wacom bash script

This was written for the Wacom Intuos Draw on Ubuntu 16.04 to configure hotkeys.

---
- Sets hotkeys and display mappings with xsetwacom. 
- Works for wired and wireless.
- Maps tablet to single display / correct ratio.
