#!/bin/bash
tablet='Wacom Intuos S 2 Pen'

function setit {
    xsetwacom set "${1} Pad pad" Button 9 key F10
    sleep 1
    xsetwacom set "${1} Pad pad" Button 8 key e
#    sleep 1
#    xsetwacom set "${1} Pen stylus" PressureCurve 0 50 50 100
    sleep 1
    xsetwacom set "${1} Pen stylus" MapToOutput eDP-1
}

if xsetwacom list | grep -q "${tablet}"; then
    setit "${tablet%\ Pen}"
else
    setit "${tablet%\ Pen} (WL)"
fi
